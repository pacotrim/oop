export class Automovel{
    protected velocidade: number;
    protected combustivel: number;
    protected velMax;

    public aceleraFreia(a: number, t: number): void{
        this.velocidade+=(a*t);
        if (this.velocidade>this.velMax){
            this.velocidade=this.velMax;
        }
    }
    public getCombustivel(): number{
        return this.combustivel;
    }
    public abastece(combustivel: number): void{
        this.combustivel+=combustivel;
    }
    public getVelocidade(): number{
        return this.velocidade;
    }
}
export class Carro extends Automovel{
    private potencia: number;
    protected tracao: string;
    private portas: number;

    public getPotencia(): number{
        return this.potencia;
    }
    public getTracao(): string{
        return this.tracao;
    }
    public getPortas(): number{
        return this.portas;
    }
    public atualizaCombustivel(distancia: number){
        this.combustivel-=(this.potencia*this.potencia)*distancia*0.7;
    }
    
    constructor(portas: number, potencia: number, velMax: number){
        super();
        this.portas=portas;
        this.potencia=potencia;
        this.velMax=velMax;
    }
}
export class FWD extends Carro{
    constructor(portas: number, potencia: number, velMax: number){
        super(portas, potencia, velMax);
        this.tracao="Fronteira";
    }
}
export class RWD extends Carro{
    constructor(portas: number, potencia: number, velMax: number){
        super(portas, potencia, velMax);
        this.tracao="Dianteira";
    }
}
export class FourWD extends Carro{
    constructor(portas: number, potencia: number, velMax: number){
        super(portas, potencia, velMax);
        this.tracao="4 rodas";
    }
}
export class Moto extends Automovel{
    private cilindradas;

    public getCilindradas(){
        return this.cilindradas;
    }

    public atualizaCombustivel(distancia: number){
        this.combustivel-=(this.cilindradas/100)*distancia*0.6;
    }
    constructor(cilindradas: number, velMax: number){
        super();
        this.cilindradas=cilindradas;
        this.velMax=velMax;
    }
}
